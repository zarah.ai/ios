import MediaPlayer
import Accelerate

fileprivate let bufferSize = 1024
fileprivate let bus = 0
fileprivate let fftBuckets = 128
fileprivate let fftConfig = vDSP_DFT_zop_CreateSetup(nil, vDSP_Length(bufferSize), vDSP_DFT_Direction.FORWARD)!
fileprivate let audioFormat = AVAudioFormat(standardFormatWithSampleRate: 44100, channels: 2)!

protocol PlayerDelegate: class {
    func player(_ player: Player, isPlaying: Bool)
    func player(_ player: Player, didUpdate amps: [Float], loudness: Float)
}

extension PlayerDelegate {
    func player(_ player: Player, isPlaying: Bool) { }
    func player(_ player: Player, didUpdate amps: [Float], loudness: Float) { }
}

class Player: NSObject, AVAudioPlayerDelegate {
    
    private let audioPlayer = AVAudioPlayerNode()
    private let audioEngine = AVAudioEngine()
    private var audioFile: AVAudioFile!
    private let processQueue = DispatchQueue.init(label: UUID().uuidString, qos: .userInteractive)
    
    weak var delegate: PlayerDelegate?
    var isPlaying: Bool { return audioPlayer.isPlaying }
    var isPrepared: Bool { return audioEngine.isRunning }
    var shouldSendMetricUpdates = true
    
    init(_: Void? = nil) throws {
        super.init()
        
        let url = Bundle.main.url(forResource: "test", withExtension: "mp3")!
        try audioFile = AVAudioFile(forReading: url)
        
        try AVAudioSession.sharedInstance().setCategory(.playback)
        try prepareAudioPlayer()
        prepareNowPlaying()
    }
    
    private func prepareAudioPlayer() throws {
        let size = AVAudioFrameCount(bufferSize)
        
        audioEngine.mainMixerNode.installTap(onBus: bus, bufferSize: size, format: nil, block: { [weak self] in self?.tap(buffer: $0, time: $1)})
        audioEngine.attach(audioPlayer)
        audioEngine.connect(audioPlayer, to: audioEngine.mainMixerNode, format: audioFormat)
        audioEngine.prepare()
        try audioEngine.start()
        
        audioPlayer.scheduleFile(audioFile, at: nil, completionHandler: nil)
    }
    
    private func prepareNowPlaying() {
        UIApplication.shared.beginReceivingRemoteControlEvents()
        
        MPRemoteCommandCenter.shared().previousTrackCommand.isEnabled = true
        MPRemoteCommandCenter.shared().previousTrackCommand.addTarget(self, action: #selector(backwardCommandPressed))
        
        MPRemoteCommandCenter.shared().nextTrackCommand.isEnabled = true
        MPRemoteCommandCenter.shared().nextTrackCommand.addTarget(self, action: #selector(forwardCommandPressed))
        
        MPRemoteCommandCenter.shared().togglePlayPauseCommand.isEnabled = true
        MPRemoteCommandCenter.shared().togglePlayPauseCommand.addTarget(self, action: #selector(playCommandPressed))
        
        let artwork = MPMediaItemArtwork(boundsSize: CGSize(width: 60, height: 60)) { _ in return UIImage(named: "Icon") ?? UIImage() }
        MPNowPlayingInfoCenter.default().nowPlayingInfo = [
            MPMediaItemPropertyTitle: "zarah.ai",
            MPMediaItemPropertyArtist: "zarah.ai",
            MPMediaItemPropertyArtwork: artwork,
            MPMediaItemPropertyMediaType: MPMediaType.music.rawValue
        ]
    }

    
    func togglePlay() throws {
        if isPlaying {
            audioPlayer.pause()
            try AVAudioSession.sharedInstance().setActive(false, options: .notifyOthersOnDeactivation)
        } else {
            try AVAudioSession.sharedInstance().setActive(true, options: .notifyOthersOnDeactivation)
            audioPlayer.play()
        }
        delegate?.player(self, isPlaying: isPlaying)
    }
    
    func skip() throws {
        
    }
    
    func previous() throws {
        
    }
    
    @objc private func playCommandPressed() -> MPRemoteCommandHandlerStatus {
        do {
            try togglePlay()
        } catch {
            return MPRemoteCommandHandlerStatus.commandFailed
        }
        return MPRemoteCommandHandlerStatus.success
    }
    
    @objc private func forwardCommandPressed() -> MPRemoteCommandHandlerStatus {
        do {
            try skip()
        } catch {
            return MPRemoteCommandHandlerStatus.commandFailed
        }
        return MPRemoteCommandHandlerStatus.success
    }
    
    @objc private func backwardCommandPressed() -> MPRemoteCommandHandlerStatus {
        do {
            try previous()
        } catch {
            return MPRemoteCommandHandlerStatus.commandFailed
        }
        return MPRemoteCommandHandlerStatus.success
    }
    

    //MARK: AudioTap
    
    private func tap(buffer: AVAudioPCMBuffer, time: AVAudioTime) {
        guard shouldSendMetricUpdates else { return }
        guard isPlaying else { return }
        guard let callback = delegate?.player(_:didUpdate:loudness:) else { return }
        
        processQueue.async { [self] in
            let amps = computeFFT(buffer)
            let loudness = computeLoudness(buffer)
            DispatchQueue.main.async { callback(self, amps, loudness) }
        }
    }
    
    private func computeLoudness(_ buffer: AVAudioPCMBuffer) -> Float {
        guard let channel = buffer.floatChannelData?[0] else { return 0 }
        let length = vDSP_Length(buffer.frameLength)
        var rms: Float = 0
        vDSP_measqv(channel, 1, &rms, length)
        
        var db = 10*log10f(rms)
        db = (40 + db)/40;
        db = max(0, min(1, db))
        return db
    }
    
    private func computeFFT(_ buffer: AVAudioPCMBuffer) -> [Float] {
        guard let channel = buffer.floatChannelData?[0] else { return Array<Float>(repeating: 0, count: fftBuckets) }
        var realIn = [Float](repeating: 0, count: bufferSize)
        var imagIn = [Float](repeating: 0, count: bufferSize)
        var realOut = [Float](repeating: 0, count: bufferSize)
        var imagOut = [Float](repeating: 0, count: bufferSize)
        
        for i in 0..<bufferSize { realIn[i] = channel[i] }
        
        vDSP_DFT_Execute(fftConfig, &realIn, &imagIn, &realOut, &imagOut)
        var complex = DSPSplitComplex(realp: &realOut, imagp: &imagOut)
        var magnitudes = [Float](repeating: 0, count: fftBuckets)
        vDSP_zvabs(&complex, 1, &magnitudes, 1, UInt(fftBuckets))
        
        var normalizedMagnitudes = [Float](repeating: 0.0, count: fftBuckets)
        var scalingFactor = 1 / Float(fftBuckets)
        vDSP_vsmul(&magnitudes, 1, &scalingFactor, &normalizedMagnitudes, 1, UInt(fftBuckets))
        
        var clippedMagnitudes = [Float](repeating: 0.0, count: fftBuckets)
        var min: Float = 0
        var max: Float = 1
        vDSP_vclip(&normalizedMagnitudes, 1, &min, &max, &clippedMagnitudes, 1, UInt(fftBuckets))
        
        return clippedMagnitudes
    }
    
}
