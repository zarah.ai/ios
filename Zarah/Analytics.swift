import Foundation
import Firebase

class Analytics {
    
    static func log(_ error: Error) {
        #if DEBUG
        print(error)
        Thread.callStackSymbols.forEach { print($0) }
        #endif
        
        Crashlytics.crashlytics().record(error: error)
    }
    
    static func log(_ event: String) {
        Crashlytics.crashlytics().log(event)
    }

}
