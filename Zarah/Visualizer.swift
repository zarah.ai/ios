import UIKit

class Visualizer: UIView {
    
    private let gradientLayer = CAGradientLayer()
    private let maskLayer = CAShapeLayer()
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        
        maskLayer.frame = bounds
        maskLayer.fillRule = .evenOdd
        
        gradientLayer.backgroundColor = UIColor(named: "AccentColor")?.cgColor
        gradientLayer.frame = bounds
        gradientLayer.mask = maskLayer
        layer.addSublayer(gradientLayer)
        
        reset()
    }
    
    private func point(_ rad: Float, distance: Float) -> CGPoint {
        let x = CGFloat(sin(rad) * distance) + bounds.width*0.5
        let y = CGFloat(cos(rad) * distance) + bounds.height*0.5
        return CGPoint(x: x, y: y)
    }
    
    func reset() {
        let starting = Array<Float>(repeating: 0, count: 128)
        visualize(starting, loudness: 0)
    }
    
    func visualize(_ amps: [Float], loudness: Float) {
        let bezier = UIBezierPath()
        let rad = Float.pi / Float(amps.count)
        let center = Float(min(bounds.width, bounds.height))
        let innerDiameter = center * 0.3 - 2 + 10 * loudness
        let outerDiameter = center * 0.3 + 2 + 10 * loudness
        
        bezier.move(to: point(0, distance: outerDiameter))
        for i in 0..<amps.count {
            let r1 = rad * Float(i+1) * 2
            let r2 = r1 - rad
            let p = point(r1, distance: outerDiameter)
            let c = point(r2, distance: outerDiameter * (1 + amps[i]))
            bezier.addQuadCurve(to: p, controlPoint: c)
        }
        bezier.close()
        
        bezier.move(to: point(0, distance: innerDiameter))
        let c = CGPoint(x: bounds.width*0.5, y: bounds.height*0.5)
        bezier.addArc(withCenter: c, radius: CGFloat(innerDiameter), startAngle: 0, endAngle: .pi * 2, clockwise: true)

        maskLayer.path = bezier.cgPath
    }

}
