import UIKit

class Controller: UIViewController, PlayerDelegate {
    
    private var player = try! Player()
    @IBOutlet private weak var playButton: UIButton!
    @IBOutlet private weak var visualizer: Visualizer!
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: "View", bundle: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        player.delegate = self
        
        NotificationCenter.default.addObserver(self, selector: #selector(didEnterBackground), name: UIApplication.didEnterBackgroundNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(willEnterForeground), name: UIApplication.willEnterForegroundNotification, object: nil)
    }
    
    @IBAction private func playButtonPressed() {
        try? player.togglePlay()
        visualizer.reset()
    }
    
    @IBAction private func backwardsButtonPressed() {
        
    }
    
    @IBAction private func fowardButtonPressed() {
        
    }
    
    @objc private func didEnterBackground() {
        player.shouldSendMetricUpdates = false
    }
    
    @objc private func willEnterForeground() {
        player.shouldSendMetricUpdates = true
    }
    
    
    func player(_ player: Player, isPlaying: Bool) {
        playButton.setTitle(isPlaying ? "" : "", for: .normal)
    }
    
    func player(_ player: Player, didUpdate amps: [Float], loudness: Float) {
        visualizer.visualize(amps, loudness: loudness)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}

